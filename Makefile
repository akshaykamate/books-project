dev:
	docker-compose -f ./deploy/docker-compose.yml -p Books up -d
down:
	docker-compose -f ./deploy/docker-compose.yml -p Books down
build:
	docker-compose -f ./deploy/docker-compose.yml -p Books build;
gitlab:
	eval $(ssh-agent -s) && \
		ssh-add -K ~/.ssh/gitlab_rsa

.DEFAULT_GOAL := dev
