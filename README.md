# Books 
An online cms to kepp the list of books to be read

## Getting started

## Features
- Basic Authentication (Register/Login with hashed password)

## setup
### Software Requirements
-  PHP >= 7.2.5
-  BCMath PHP Extension
-  Ctype PHP Extension
-  Fileinfo PHP extension
-  JSON PHP Extension
-  Mbstring PHP Extension
-  OpenSSL PHP Extension
-  PDO PHP Extension
-  Tokenizer PHP Extension
-  XML PHP Extension

### Install composer dependencies after installing (Git or manual download) 
```bash
$ make build
```
### Setting up environments

1.  You will find a file named `.env.example` on root directory of project.
2.  Create a new file by copying and pasting the file and then renaming it to just `.env`
    ```bash
    cp ./backend/.env.example ./backend/.env
    ```
3.  The file `.env` is already ignored, so you never commit your credentials.
4.  Change the values of the file to your environment. Helpful comments added to `.env.example` file to understand the constants.

## Project structure

## How to run

### Running API server locally

```bash
make dev
```

You will know server is running by checking the output of the command `make dev`

```bash
Connected to mongodb:YOUR_DB_CONNECTION_STRING
App is running ...

Press CTRL + C to stop the process.
```

**Note:** `YOUR_DB_CONNECTION_STRING` will be your MongoDB connection string.


## Tests

### Running Test Cases

```bash
make test
```

## Bugs or improvements
Every project needs improvements, Feel free to report any bugs or improvements. Pull requests are always welcome.

## License
This project is open-sourced software licensed under the MIT License. See the `LICENSE` file for more information.
