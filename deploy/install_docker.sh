#!/bin/bash

echo "Docker installation - Start"
echo "+++++++++++++++++++++++++++++++++++++++++++"
apt-get update

apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

echo "+++++++++++++++++++++++++++++++++++++++++++\
check status \
+++++++++++++++++++++++++++++++++++++++++++\
pub   rsa4096 2017-02-22 [SCEA] \
    9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88 \
uid           [ unknown] Docker Release (CE deb) <docker@docker.com>\
sub   rsa4096 2017-02-22 [S]\
+++++++++++++++++++++++++++++++++++++++++++"
sudo apt-key fingerprint 0EBFCD88
sleep 5
add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y  docker-ce docker-ce-cli containerd.io
echo "+++++++++++++++++++++++++++++++++++++++++++\
check status \
+++++++++++++++++++++++++++++++++++++++++++"
systemctl status docker
echo "+++++++++++++++++++++++++++++++++++++++++++"
sleep 5

groupadd docker
usermod -aG docker akshays
newgrp docker
docker run hello-world
echo "+++++++++++++++++++++++++++++++++++++++++++
Docker installation  - Complete\
+++++++++++++++++++++++++++++++++++++++++++"
