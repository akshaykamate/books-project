#!/bin/bash

mkdir -p /var/jenkins_home
chown -R 1000:1000 /var/jenkins_home/
docker build -f ./Dockerfile_jenkins -t jenkins-docker .
docker run -p 8080:8080 -p 50000:50000 -v /var/jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -d --name jenkins jenkins-docker

echo "jenkins installed"
echo "check for access jenkins @ http://"$(curl -s ifconfig.co);
echo "password:"
cat  /var/jenkins_home/secrets/initialAdminPassword

