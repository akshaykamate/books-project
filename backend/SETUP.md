# Docker commands for backend

Database migration
```bash
docker exec php php artisan migrate
```
Encryption keys generate
```bash
docker exec php php artisan passport:keys
```
Personal access and Password grant client create
```bash
docker exec php php artisan passport:install
```
docker list routes
```bash
docker exec php php artisan route:list
```
